Classification Task: Built classification model based on a given dataset to classify untrained data using SVM, Logistic Regression, K-neighbour classifier, Random Forest classifier. Load and split data into training and test data. Calculate accuracy for training and test data. Evaluate training accuracy vs test accuracy. SVM has high accuracy rate with 97%.
Keyword: Kernel trick.
