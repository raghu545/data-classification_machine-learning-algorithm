import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

%matplotlib inline
df=pd.read_csv(r'E:\python-project\TrainingSamplesLabeled.csv',header=None,names=["A","B","C","D","E","F","G"])
df.head(5)
df.info()
df.describe()
sns.pairplot(data=df)
df['G'].value_counts()
df.columns
from sklearn.cross_validation import train_test_split

X=df[['A', 'B', 'C', 'D', 'E', 'F']]
y=df['G']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

X_train.head(5)

y_train.head(5)

from sklearn.svm import SVC

model=SVC(kernel='linear')

model.fit(X_train,y_train)  

predictions=model.predict(X_test)

from sklearn.metrics import confusion_matrix,accuracy_score,classification_report

print(accuracy_score(y_test,predictions))

print(confusion_matrix(y_test,predictions))

print(classification_report(y_test,predictions))

param_grid = {'C': [0.1,1, 10, 100, 1000], 'gamma': [1,0.1,0.01,0.001,0.0001]} 
from sklearn.grid_search import GridSearchCV
grid=GridSearchCV(SVC(),param_grid,refit=True,verbose=3)
grid.fit(X_train,y_train)
grid.best_params_
grid_predictions = grid.predict(X_test)
print(accuracy_score(y_test,grid_predictions))
print(confusion_matrix(y_test,grid_predictions))
print(classification_report(y_test,grid_predictions))
X=df[['A', 'B', 'C', 'D', 'E', 'F']]
y=df['G']
from sklearn.cross_validation import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0, random_state=101)
from sklearn.svm import SVC
model=SVC(C=1, kernel='linear')
model.fit(X_train,y_train)
testdata=pd.read_csv("E:\\python-project\\AllSamplesUnlabeled.csv",header=None)
testdata.head(5)
predict=model.predict(testdata)
df_predvalues = pd.DataFrame(data=predict)
df_predvalues.head(3)
df_Result=pd.concat([testdata,df_predvalues],axis=1)
df_Result.head(2)
df_Result.to_csv(r'E:\python-project\SVM-SVC\Result.csv',header=["A","B","C","D","E","F","PredValue"],index=False)
